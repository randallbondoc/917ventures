# [Randall Anthony Bondoc](https://www.linkedin.com/in/randall-anthony-bondoc-aa95b9139/) 917Ventures Exam

A basic user management application made with [Laravel](https://laravel.com/) and [Vue.js](https://vuejs.org/) implementing authentication (login/registration - implements [laravel/ui](https://github.com/laravel/ui)), CRUD functions wherein admin can:

* Add a new user
* View a user
* Edit a user
* Delete a user
* View list of all users in the system (with pagination, search, sorting - implements [vue-database-net](https://github.com/niiknow/vue-datatables-net))
* Allow multiple users to be removed
* Form validations ([vuelidate](https://vuelidate.js.org/))

## Getting started

1. [Clone repository](git clone git@bitbucket.org:randallbondoc/917ventures.git).
2. Run "composer install".
3. Create a database and then modify the .env file.
4. Run "php artisan key:generate".
5. Run "php artisan migrate:refresh --seed".
6. Run "php artisan serve".
7. Run the application "http://127.0.0.1:8000".

#### Notes

You don't need to run npm install on the new server if you will not be modifying any scripts or styles. But if you may need to, just run "npm install"


## Built With

* [Laravel](https://laravel.com/)
* [Vue.js](https://vuejs.org/)
* [Bootstrap 4](https://getbootstrap.com/)
* [NPM](https://www.npmjs.com/)
* [jQuery](https://jquery.com/)
* [vuelidate](https://vuelidate.js.org/)
* [vue-database-net](https://github.com/niiknow/vue-datatables-net)
* [vue-axios](https://github.com/imcvampire/vue-axios)
* [vue-sweetalert2](https://avil13.github.io/vue-sweetalert2/)
* [vue-fontawesome](https://github.com/FortAwesome/vue-fontawesome)

#### Versions

-PHP version ^7.2.5

-Laravel Framework 7.x