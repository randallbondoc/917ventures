<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param User $user_model
     * @param UserRepository $user_repository
     *
     * @return void
     */
    public function __construct(User $user_model, UserRepository $user_repository)
    {
        $this->middleware('auth', ['except' => ['index']]);

        /*
        * Model namespace
        * using $this->user_model can also access $this->user_model->where('id', 1)->get();
        * */
        $this->user_model = $user_model;

        /*
        * Repository namespace
        * this class may include methods that can be used by other controllers, like getting of posts with other data (related tables).
         * */
        $this->user_repository = $user_repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->check()) {
            $users = $this->user_model->get();
            return view('pages.user.index', compact('users'));
        } else {
            return view('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->user_repository->getById($id);

        return view('pages.user.show', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'status' => false,
            'messages' => [
                'swal' => [
                    'icon' => 'error',
                    'title' => 'Error!',
                    'text' => 'Something went wrong!',
                ]
            ],
            'data' => [],
        ];

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'post_code' => ['required', 'string', 'max:255'],
            'contact_number' => ['required', 'string', 'max:255'],
            'user_name' => ['required', 'string', 'max:255', 'unique:users,user_name,NULL,id,deleted_at,NULL'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,NULL,id,deleted_at,NULL'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = $this->user_model->create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'address' => $request['address'],
            'post_code' => $request['post_code'],
            'contact_number' => $request['contact_number'],
            'user_name' => $request['user_name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);

        $response['status'] = true;
        $response['messages']['swal'] = [
            'icon' => 'success',
            'title' => 'Success!',
            'text' => 'User successfully added.',
            'redirect_route' => route('admin.users.index')
        ];
        $response['data'] = $user;

        return json_encode($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user_repository->getById($id);

        return view('pages.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = [
            'status' => false,
            'messages' => [
                'swal' => [
                    'icon' => 'error',
                    'title' => 'Error!',
                    'text' => 'Something went wrong!',
                ]
            ],
            'data' => [],
        ];

        $user = $this->user_repository->getById($id);

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'post_code' => ['required', 'string', 'max:255'],
            'contact_number' => ['required', 'string', 'max:255'],
            'user_name' => ['required', 'string', 'max:255', 'unique:users,user_name,' . $id . ',id,deleted_at,NULL'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $id . ',id,deleted_at,NULL'],
            'password' => 'exclude_if:change_password,false|string|min:8|confirmed',
        ]);

        if ($request->get('change_password')) {
            $input = $request->except(['change_password']);
        } else {
            $input = $request->except(['change_password', 'password']);
        }

        $user->fill($input)->save();

        $response['status'] = true;
        $response['messages']['swal'] = [
            'icon' => 'success',
            'title' => 'Success!',
            'text' => 'User successfully updated.',
            'redirect_route' => route('admin.users.index')
        ];
        $response['data'] = $user;

        return json_encode($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user_repository->getById($id);

        $user->delete();

        $response['status'] = true;
        $response['messages']['swal'] = [
            'icon' => 'success',
            'title' => 'Success!',
            'text' => 'User successfully deleted.',
            'redirect_route' => route('admin.users.index')
        ];
        $response['data'] = [];

        return json_encode($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroySelected(Request $request)
    {
        $users = $this->user_model->whereIn('id', $request->get('ids'))->get()->each(function ($user, $key) {
            $user->delete();
        });

        $response['status'] = true;
        $response['messages']['swal'] = [
            'icon' => 'success',
            'title' => 'Success!',
            'text' => 'User(s) successfully deleted.',
            'redirect_route' => route('admin.users.index')
        ];
        $response['data'] = [];

        return json_encode($response);
    }

    /**
     * Datatables draw
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function draw(Request $request)
    {
        $columns = array(
            0 => 'checkbox',
            1 => 'id',
            2 => 'name',
            3 => 'user_name',
            4 => 'email',
            54 => 'action',
        );

        $totalData = $this->user_model
            ->select('users.*', (DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) as name")))
            ->count();

        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            if ($limit == -1) {
                $users = $this->user_model
                    ->select('users.*', (DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) as name")))
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $users = $this->user_model
                    ->select('users.*', (DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) as name")))
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            }
        } else {
            $search = $request->input('search.value');

            if ($limit == -1) {
                $users = $this->user_model
                    ->orWhere('user_name', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%")
                    ->orWhere((DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`)")), 'LIKE', "%{$search}%")
                    ->select('users.*', (DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) as name")))
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $users = $this->user_model
                    ->orWhere('user_name', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%")
                    ->orWhere((DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`)")), 'LIKE', "%{$search}%")
                    ->select('users.*', (DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) as name")))
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            }

            $totalFiltered = $this->user_model
                ->orWhere('user_name', 'LIKE', "%{$search}%")
                ->orWhere('email', 'LIKE', "%{$search}%")
                ->orWhere((DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`)")), 'LIKE', "%{$search}%")
                ->select('users.*', (DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) as name")))
                ->orderBy($order, $dir)
                ->count();
        }

        $data = [];
        if (!empty($users)) {
            foreach ($users as $user) {
                $nestedData['id'] = $user->id;
                $nestedData['name'] = $user->first_name . ' ' . $user->last_name;
                $nestedData['user_name'] = $user->user_name;
                $nestedData['email'] = $user->email;
                $nestedData['links'] = [
                    'show' => route('admin.users.show', $user->id),
                    'edit' => route('admin.users.edit', $user->id)
                ];

                $data[] = $nestedData;
            }
        }

        $response = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
            "request" => $request->all()
        );

        return json_encode($response);
    }
}
