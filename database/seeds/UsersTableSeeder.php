<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        User::truncate();
        foreach (range(1, 25) as $i) {
            User::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'address' => $faker->address,
                'post_code' => $faker->postcode,
                'contact_number' => $faker->phoneNumber,
                'user_name' => $faker->userName,
                'email' => $faker->unique()->email,
                'password' => 'P@ssword1',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ]);
        }
    }
}


