<template>
    <div>
        <div class="row">
            <div class="col-12 float-right">
                <div class="input-group">
                    <a href="javascript:void(0)" v-if="hasSelected" @click="actionDeleteSelected"
                       class="btn btn-danger">
                        <i class="fa fa-times"></i> Delete Selected</a>
                </div>
            </div>
        </div>
        <!-- Using the VdtnetTable component -->
        <vdtnet-table
                ref="table"
                :fields="fields"
                :opts="options"
                :containerClassName="containerClassName"
                :select-checkbox="1"
                :errMode="errMode"
                @show="actionShow"
                @edit="actionEdit"
                @delete="actionDelete"
                @reloaded="doAfterReload"
                @row-select="handleRowSelect"
                @row-deselect="handleRowSelect"
        >
        </vdtnet-table>
    </div>
</template>

<script>
    import VdtnetTable from 'vue-datatables-net'

    import 'datatables.net-bs4'

    import 'datatables.net-buttons/js/dataTables.buttons.js'
    import 'datatables.net-buttons/js/buttons.html5.js'
    import 'datatables.net-buttons/js/buttons.print.js'

    import 'datatables.net-buttons-bs4'
    import 'datatables.net-select-bs4'
    import 'datatables.net-select-bs4/css/select.bootstrap4.min.css'
    import 'datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css'

    export default {
        components: {VdtnetTable},
        props: {
            user: Object
        },
        data() {
            const vm = this
            return {
                selectedIds: [],
                hasSelected: false,
                containerClassName: 'd-print-inline',
                errMode: "throw",
                options: {
                    errMode: '',
                    ajax: {
                        url: '/admin/users/draw',
                        dataSrc: (json) => {
                            return json.data
                        }
                    },
                    /*eslint-disable */
//                    dom: "Blrt<'row vdtnet-footer'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    dom: "<'row'<'col-sm-6 col-xs-5'l><'col-sm-6 col-xs-7 text-right'f><'col-12'r>>t<'row'<'col-sm-4 col-xs-4'i><'col-sm-8 col-xs-8 float-right'p>>",
                    oLanguage: {
                        sLengthMenu: "_MENU_",
                        sSearch: "<div class=\"input-group\">_INPUT_<div class=\"input-group-append\">" +
                        "<span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span></div></div>",
                        sInfo: "<strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
//                        oPaginate: {
//                            sFirst: "",
//                            sLast: "",
//                            sPrevious: "",
//                            sNext: ""
//                        },
                        sProcessing: "<div class='col-sm-12'><div class='alert alert-info alert-dismissable image-empty'><b><i class='fa fa-spinner fa-pulse'></i> Loading..</b></div></div>"
                    },
                    sWrapper: "dataTables_wrapper form-inline",
                    sFilterInput: "form-control test",
                    sLengthSelect: "form-control",
                    /*eslint-enable */
                    pageLength: 10,
                    lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']],
                    select: {style: 'multi'},
                    responsive: false,
                    processing: true,
                    searching: true,
                    searchDelay: 1000,
                    destroy: true,
                    ordering: true,
                    lengthChange: true,
                    serverSide: true,
                    fixedHeader: true,
                    saveState: true,
                },
                fields: {
                    id: {label: 'ID', sortable: true, defaultOrder: 'asc', className: 'text-center',},
                    name: {label: 'Name', sortable: true, searchable: true},
                    user_name: {label: 'Username', sortable: true, searchable: true},
                    email: {label: 'Email', sortable: true, searchable: true},
                    actions: {
                        className: 'text-center',
                        isLocal: true,
                        label: 'Actions',
                        defaultContent: '<div class="btn-group btn-group-xs" role="group">' +
                        '<a href="javascript:void(0);" data-action="show" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>' +
                        '<a href="javascript:void(0);" data-action="edit" class="btn btn-default btn-sm"><i class="fa fa-edit"></i> </a>' +
                        '<span data-action="delete" class="btn btn-danger btn-sm"><i class="fa fa-times""></i> </span>' +
                        '</div>'
                    },
                },
                quickSearch: '',
            }
        },
        methods: {
            handleRowSelect() {
                this.selectedIds = [];
                const allSelectedRows = this.$refs.table.dataTable.rows({selected: true}).data()
                for (let i = 0; i < allSelectedRows.length; i++) {
                    this.selectedIds.push(allSelectedRows[i].id)
                }
                if (this.selectedIds.length) {
                    this.hasSelected = true
                } else {
                    this.hasSelected = false
                }
            },
            actionShow(data) {
                location.href = data.links.show;
            },
            actionEdit(data) {
                location.href = data.links.edit;
            },
            actionDelete(data, row, tr, target) {
                Vue.swal({
                    title: "Are you sure?",
                    html: "Are you sure you want to delete this record?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: (isConfirm) => {
                        return axios
                            .delete('/admin/users/' + data.id + '/delete')
                            .then(response => {
                                self.sendRequest = false;
                                if (response.data.messages.hasOwnProperty('swal')) {
                                    var arrSwal = response.data.messages.swal;
                                    Vue.swal({
                                        icon: arrSwal.icon,
                                        title: arrSwal.title,
                                        text: arrSwal.text,
                                    }).then((result) => {
                                        const table = this.$refs.table
                                        table.reload()
                                    })
                                }
                            })
                            .catch(error => {
                                if (response.data.messages.hasOwnProperty('swal')) {
                                    var arrSwal = response.data.messages.swal;
                                    Vue.swal({
                                        icon: arrSwal.icon,
                                        title: arrSwal.title,
                                        text: arrSwal.text,
                                    }).then((result) => {})
                                }
                            });
                    },
                    allowOutsideClick: () => !Vue.swal.isLoading()
                }).then((result) => {})
            },
            actionDeleteSelected(data, row, tr, target) {
                Vue.swal({
                    title: "Are you sure?",
                    html: "Are you sure you want to delete these record(s)?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete them!',
                    showLoaderOnConfirm: true,
                    preConfirm: (isConfirm) => {
                        return axios
                            .post('/admin/users/delete-selected', {
                                'ids': this.selectedIds
                            })
                            .then(response => {
                                self.sendRequest = false;
                                if (response.data.messages.hasOwnProperty('swal')) {
                                    var arrSwal = response.data.messages.swal;
                                    Vue.swal({
                                        icon: arrSwal.icon,
                                        title: arrSwal.title,
                                        text: arrSwal.text,
                                    }).then((result) => {
                                        const table = this.$refs.table
                                        table.reload()
                                    })
                                }
                            })
                            .catch(error => {
                                if (response.data.messages.hasOwnProperty('swal')) {
                                    var arrSwal = response.data.messages.swal;
                                    Vue.swal({
                                        icon: arrSwal.icon,
                                        title: arrSwal.title,
                                        text: arrSwal.text,
                                    }).then((result) => {})
                                }
                            });
                    },
                    allowOutsideClick: () => !Vue.swal.isLoading()
                }).then((result) => {})
            },
            doAfterReload(data, table) {
                this.handleRowSelect()
            },
            doSearch() {
                this.$refs.table.search(this.quickSearch)
            },
        }
    }
</script>

<style scoped>
</style>