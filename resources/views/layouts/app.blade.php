<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.header')
<body>
<div id="app">
    @include('layouts.nav')
    <main class="">
        @yield('content')
    </main>
</div>

@include('layouts.footer')

</body>
</html>
