<!-- Site footer -->
<footer class="site-footer">
        <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; {{ date('Y') }} All Rights Reserved by
                    <a target="_blank" href="https://www.linkedin.com/in/randall-anthony-bondoc-aa95b9139/">Randall Anthony Bondoc</a>.
                </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    <li><a class="google" target="_blank" href="mailto:randallbondoc@gmail.com"><i class="fab fa-google"></i></a></li>
                    <li><a class="linkedin" target="_blank" href="https://www.linkedin.com/in/randall-anthony-bondoc-aa95b9139/"><i class="fab fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>