@extends('layouts.app')

@section('content')
    <div class="container container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header clearfix">
                        <h2 class="float-left"><i class="fa fa-users"></i>&nbsp;<strong>Users</strong></h2>
                        <a href="{{ route('admin.users.create') }}" class="btn btn-success float-right">
                            <i class="fa fa-plus"></i> Add New User
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="alert alert-info alert-dismissable user-empty {{$users->count() == 0 ? '' : 'd-none' }}">
                                <i class="fa fa-info-circle"></i> No users found.
                            </div>
                            <user-list :users="{{ $users }}" :user="{{ auth()->user() }}"></user-list>
                            <!--
                            <div class="table-responsive {{$users->count() == 0 ? 'd-none' : '' }}">
                                <table id="users-table"
                                       class="table table-bordered table-striped table-vcenter">
                                    <thead>
                                    <tr role="row">
                                        <th class="text-left">
                                            Name
                                        </th>
                                        <th class="text-left">
                                            Username
                                        </th>
                                        <th class="text-left">
                                            Email
                                        </th>
                                        <th class="text-center">
                                            Action
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr data-user-template-id="{{$user->id}}">
                                            <td class="text-left">
                                                <strong>{{ $user->first_name.' '.$user->last_name }}</strong></td>
                                            <td class="text-left">{{ $user->user_name    }}</td>
                                            <td class="text-left">{{ $user->email }}</td>
                                            <td class="text-center">
                                                <div class="btn-group btn-group-sm" role="group">
                                                    <a href="{{ route('admin.users.show', $user->id) }}"
                                                       data-toggle="tooltip"
                                                       title=""
                                                       class="btn btn-default"
                                                       data-original-title="View"><i class="fa fa-eye"></i></a>
                                                    <a href="{{ route('admin.users.edit', $user->id) }}"
                                                       data-toggle="tooltip"
                                                       title=""
                                                       class="btn btn-default"
                                                       data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                                       title=""
                                                       class="btn btn-xs btn-danger delete-user-btn"
                                                       data-original-title="Delete"
                                                       data-user-id="{{ $user->id }}"
                                                       data-user-route="{{ route('admin.users.delete', $user->id) }}">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
