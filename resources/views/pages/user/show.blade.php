@extends('layouts.app')

@section('content')
    <div class="container container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
                <li class="breadcrumb-item active"
                    aria-current="page">{{ $user->first_name . ' ' . $user->last_name }}</li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header clearfix">
                        <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-default float-right">
                            <i class="fa fa-edit"></i> Edit User
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="block-section text-center">
                            <h3>
                                <strong>{{ $user->first_name.' '.$user->last_name }}</strong><br>
                                <small></small>
                            </h3>
                        </div>
                        <table class="table table-borderless table-striped table-vcenter">
                            <tbody>
                            <tr>
                                <td class="text-right"><strong>Username</strong></td>
                                <td>{{ $user->user_name }}</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Email</strong></td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Address</strong></td>
                                <td>{!! $user->address !!}</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Postcode</strong></td>
                                <td>{{ $user->post_code }}</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Contact Number</strong></td>
                                <td>{{ $user->contact_number }}</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Creation Date</strong></td>
                                <td>{{ $user->created_at->format('F d, Y h:i:s a') }}</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Last Visit</strong></td>
                                <td>{{ date('F d, Y h:i:s a', strtotime($user->last_login)) }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection