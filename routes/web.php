<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');


//Auth::routes();

/* Login Routes */
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::post('/logout', ['as' => 'logout.post', 'uses' => 'Auth\LoginController@logout']);

/* Registration Routes */
Route::get('/register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('/register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);

/* Forgot Password Routes */
//Route::get('/password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
//Route::post('/password/email', ['as' => 'password.email.post', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
//Route::get('/password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
//Route::post('/password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

Route::group([
    "prefix" => 'admin'
], function () {

    Route::get('/', function () {
        return redirect('/admin/users');
    });

    /* users */
    Route::get('/users/draw',
        ['as' => 'admin.users.draw',
            'uses' => '\App\Http\Controllers\UserController@draw']
    );

    Route::resource('/users', 'UserController', [
        'as' => 'admin'
    ]);

    Route::delete('/users/{id}/delete',
        ['as' => 'admin.users.delete',
            'uses' => '\App\Http\Controllers\UserController@destroy']
    );

    Route::post('/users/delete-selected',
        ['as' => 'admin.users.delete-selected',
            'uses' => '\App\Http\Controllers\UserController@destroySelected']
    );
    /* users */

});
